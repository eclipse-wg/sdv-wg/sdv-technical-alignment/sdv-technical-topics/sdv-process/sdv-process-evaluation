
# SDV Process Evaluation

This project gathers information about the SDV Process Evaluation.

It is meant as the implementation of the specifications defined in its sister project, [SDV Process Definition](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/sdv-process/sdv-process-definition).

The repository is organised as follows:
* Per-project configuration files for manual criteria in the evaluation (see `conf/`). This directory defines what projects (and components) are included in the maturity assessment program. 
* Per-project assets storage, to archive artefacts used in the maturity assessment process (see `assets/`). 

## Evaluation workflow

The evaluation is executed daily, during the [Metrics website](https://metrics.eclipse.org) generation. The evaluation process is sequenced as follows:

* For each project submitted to the assessment, there is a [file called sdv-manifests.yml](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/sdv-process/sdv-process-evaluation/-/blob/main/assets/automotive.uprotocol/sdv-manifests.yml), stored in the Eclipse-controlled [sdv-process-evaluation repository](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/sdv-process/sdv-process-evaluation/). This file lists all components for the project - i.e. repositories that have set up the [quevee action](https://github.com/eclipse-dash/quevee) in their release process. The naming convention for the location of the file is `/assets/<project_id>/sdv-manifests.yml`.
* When executed, the assessment script reads this file for each project, then for each component of the project fetches the `sdv-manifest.toml` file from the assets of the repo's last release.
* The content of the file is used to do the actual computation of the badges, as can be seen e.g. for [Eclipse uProtocol / up-rust](https://metrics.eclipse.org/projects/automotive.uprotocol/#tabs-project_basics-5).
* For projects that do not have a sdv-manifests.yml file yet (i.e. projects without the proper quevee release setup), we still compute available criteria - e.g. documentation, oss best practices, blueprints integrations. An example can be seen with [Eclipse Ankaios](https://metrics.eclipse.org/projects/automotive.ankaios/#tabs-project_basics-5).

