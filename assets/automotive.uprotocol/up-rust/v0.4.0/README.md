
This directory contains the assets used for the maturity assessment of automotive.uprotocol / up-rust / v0.4.0.

For more information, please see:
* The [SDV assessment definition](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/sdv-process/sdv-process-definition) repository.
* The [PMI page](https://projects.eclipse.org/projects/automotive.uprotocol) of the project.
* The [URL of the targeted release](https://github.com/eclipse-uprotocol/up-rust/releases/tag/v0.4.0).

This page was generated on Fri Feb 28 17:14:05 2025.
                    